
import './App.css';
import UserList from './UserList'
import leaflet  from 'leaflet'
import 'leaflet/dist/leaflet.css';


function App() {
  
  
  
  return (
    <div className="App">
      <UserList />
    </div>
  );
}

export default App;
