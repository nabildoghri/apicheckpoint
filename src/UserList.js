import axios from 'axios';
import {useState,useEffect} from 'react'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'

function  MapComponent({lat,lan}){
    return <MapContainer center={[lan,lat]} zoom={5}  >
    <TileLayer
      attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />
    <Marker position={[lan,lat]}>
      <Popup>
        A pretty CSS3 popup. <br /> Easily customizable.
      </Popup>
    </Marker>
  </MapContainer>
        
}

function UserLine({user}){
    return     <div style={{padding :"20px", margin :"10px", "backgroundColor":"lightGray" }} >
    <div className="container">
         <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-6">
            <div className="well well-sm">
                <div className="row">
                    <div className="col-sm-8 col-md-8">
                    <MapComponent lat={user.address.geo.lat} lan={user.address.geo.lng}/>
                    </div>
                    <div className="col-sm-4 col-md-4">
                        <h4>
                        {user.name}</h4>
                        <small><cite title="San Francisco, USA">{user.address.street},{user.address.zipcode}, {user.address.suite}<i className="glyphicon glyphicon-map-marker">
                        </i></cite></small>
                        <p>
                            <i className="glyphicon glyphicon-envelope"></i>{user.email}
                            <br />
                            <i className="glyphicon glyphicon-globe"></i><a href={user.website}>{user.website}</a>
                            <br />
                            </p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    </div>

}
function UserList(){
    const [userList, setUserList] = useState([])
    useEffect(async () => {
        const result = await axios(
          'https://jsonplaceholder.typicode.com/users',
        )
     
        setUserList(result.data);
      },[]);

    var userArray = []
    userList.forEach((user)=>{  userArray.push(<UserLine user={user} key={user.id}/>)})
    return <div>
        
        {userArray}
    </div>

}

export default UserList